﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWorkIno
{
    class Card
    {
        private int _id=-1;
        private int _level = 0;
        private string _userName = "NaN";
        private string _key = "12#fds1d";

        public int ID
        {
            get { return _id; }
            set { _id = value; }
        }
        public int level
        {
            get { return _level; }
            set { _level = value; }
        }
        public string userName
        {
            get { return _userName; }
            set { _userName = value; }
        }

        public string key
        {
            get { return _key; }
            set { _key = value; }
        }
    }
}
