﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWorkIno
{
    interface Iterminal
    {
        int machineId { get; set; }
        string location { get; set; }
        bool isInWork { get; }

        Dictionary<Card, DateTime> getAccessLog(Card user);
        bool getAccess(Card user);
    }
}
