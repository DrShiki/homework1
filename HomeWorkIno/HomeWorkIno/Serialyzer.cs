﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWorkIno
{
    class Serialyzer
    {
        private static Serialyzer _instance = null;
        Dictionary<int, Card> _cardList = new Dictionary<int, Card>();
        List<int> _banList;
        private Serialyzer()
        { }
        private string _generateCode(string cardHolder)
        {
            StringBuilder code = new StringBuilder(cardHolder);
            for (int i = 0; i < code.Length; i++)
                code[i] += (char)(new Random(3)).Next(0, 2);
            return code.ToString();
        }
        public void onBanned(DateTime accessTime, Card userCard, string userEvent)
        {
            Console.WriteLine(accessTime.ToString() + " " + userCard.userName +" "+ userEvent);

        }
        public Card registrateCard(string cardHolder,int accessLevel)
        {
            if (_cardList.Where(x => x.Value.userName == cardHolder).Count() != 0)
                throw new Exception("Card which such cardHolder already exists!");
            Card newOne = new Card();
            newOne.userName = cardHolder;
            newOne.level = accessLevel;
            newOne.ID = _cardList.Count + 1;
            newOne.key = _generateCode(newOne.userName);
            _cardList.Add(newOne.ID, newOne);
            return newOne;
        }
        public static Serialyzer getInstance()
        {
            if (_instance == null)
                _instance = new Serialyzer();
            return _instance;
        }
        public bool checkCard(Card obj)
        {
            var cardsById = _cardList.Where(x => x.Value.ID== obj.ID);
            if (cardsById.Count() != 0)
            {
                var cardById = cardsById.Last().Value;
                bool cmp = cardById?.key == obj.key && cardById?.level == obj.level && cardById?.userName == obj.userName;
                if (cmp)
                    return true;
                else
                    return false;
            }
            else
                return false;

        }
    }
}
