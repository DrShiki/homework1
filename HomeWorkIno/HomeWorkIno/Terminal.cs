﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWorkIno
{
    class Terminal:Iterminal
    {
        private static List<int> _idList = new List<int>();
        private Dictionary<Card, DateTime> _accesLog;
        private string _location;
        private int _id;
        private bool _isOk;
        public delegate void accessEvent(DateTime accessTime,Card userCard,string userEvent);
        public event accessEvent AccessEvent;
        public delegate void invalidAccsess(DateTime accessTime, Card userCard, string userEvent);
        public event invalidAccsess InvalidAccsess;
        public int machineId
        {
            get
            {
                return _id;
            }
            set
            {
                if (_idList.Where(i => i == value).Count() == 0)
                {
                    _id = value;
                    _idList.Add(_id);
                }
                else
                    throw new Exception("ID already in use");
            }
        }
        public string location
        {
            get { return _location; }
            set { _location = value; }
        }
        public bool isInWork
        {
            get { return _isOk; }
        }
        public Dictionary<Card, DateTime> getAccessLog(Card user)
        {
            AccessEvent?.Invoke(DateTime.Now, user, "Trying to get access to private information(LOG)!");
            if (Serialyzer.getInstance().checkCard(user)&&user.level>4)
                return _accesLog;
            else
                throw new Exception("Invalid level for this operation!");
        }
        public bool getAccess(Card user)
        {
            AccessEvent?.Invoke(DateTime.Now, user, "Trying to enter in system!");
            if(Serialyzer.getInstance().checkCard(user))
                return true;
            else
            {
                InvalidAccsess(DateTime.Now, user, "Incorrect card used");
                return false;
            }
        }
    }
}
